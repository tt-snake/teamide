package util

import "errors"

var (
	ProgressCallStoppedError = errors.New("progress call stopped")
)
